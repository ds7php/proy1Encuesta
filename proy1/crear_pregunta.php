
<HTML LANG="es">
<HEAD>
 <TITLE>Proyecto #1 - Registrar Pregunta</TITLE>
     <LINK REL="stylesheet" TYPE="text/css" HREF="css/estilo.css">
</HEAD>
<BODY>
<H1>Registro de nueva pregunta</H1>
<?php
require_once("class/encuesta_mant.php");
if(array_key_exists('enviar', $_POST))
{
    $valor_preg = $_REQUEST['pregunta'];
    $tipo_preg = $_REQUEST['tipo'];
    $obj_mantenimiento = new mantenimiento();
    $result = $obj_mantenimiento->insertar_pregunta($valor_preg, $tipo_preg);
    if($result){
        print ("<P ALIGN='CENTER'> Se ha insertado tu pregunta correctamente</P>\n");
        print ("<P ALIGN='CENTER'>[ <A HREF='mantenimiento.php'>Volver a la pantalla de mantenimiento</A> ]</P>\n");
    }
    else{
        print ("<A HREF='mantenimiento.php'>Error al insertar la pregunta</A>\n");
    }
}
else
{
?>

<FORM NAME="formnPreg" METHOD="post" ACTION="crear_pregunta.php">
<H2>Ingresa los datos de tu pregunta</H2>
<H3>Pregunta:</H3>
<INPUT TYPE="text" NAME="pregunta" SIZE="155">
<BR/>
<H3>¿De que forma se puede responder esta pregunta?</h3>
<INPUT TYPE="RADIO" NAME="tipo" VALUE="1" CHECKED>Seleccion binaria (Si - No)<BR>
<INPUT TYPE="RADIO" NAME="tipo" VALUE="2">Seleccion única (Nada Importante - Demasiado Importante)<BR>
<INPUT TYPE="RADIO" NAME="tipo" VALUE="3">Seleccion Multiple (Marcas de Celulares)<BR>
<INPUT TYPE="RADIO" NAME="tipo" VALUE="4">Seleccion única (De 4 a 8 meses - De 5 a 10 años)<BR>
<INPUT TYPE="RADIO" NAME="tipo" VALUE="5">Seleccion Multiple (Features del Equipo [Carga, Camara, etc.])<BR>
<INPUT TYPE="RADIO" NAME="tipo" VALUE="6">Seleccion Multiple (Accesorios [audifono, cargador, etc...])<BR>
<BR><INPUT TYPE="SUBMIT" NAME="enviar" VALUE="Registrar Pregunta">
</FORM>
<?php
}
?>
</BODY>
</HTML>
