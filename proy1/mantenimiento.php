<HTML LANG="es">
<HEAD>
 <TITLE>Proyecto #1 - Mantenimiento</TITLE>
     <LINK REL="stylesheet" TYPE="text/css" HREF="css/estilo.css">
</HEAD>
<BODY>
<H1>Pantalla de Mantenimiento - Encuesta</H1>
<FORM NAME="FormFiltro" METHOD="post" ACTION="mantenimiento.php">
<BR/>
Filtrar por <SELECT NAME="campos">
<OPTION value="id">ID
<OPTION value="pregunta">Pregunta
<OPTION value="tipo_pregunta">Tipo pregunta
</SELECT>
con el valor
<input TYPE="text" NAME="valor">
<INPUT NAME="ConsultarFiltro" VALUE="Filtrar Datos" TYPE="submit" />
<INPUT NAME="ConsultarTodos" VALUE="Ver Todos" TYPE="submit" />
</FORM>
<?PHP 

require_once("class/encuesta_mant.php");

if (!empty($_REQUEST['elim']))
{    
    $elim = $_REQUEST['elim'];
    $obj_mantenimiento = new mantenimiento();
    $obj_mantenimiento->eliminar_pregunta($elim);
}

$obj_mantenimiento = new mantenimiento();
$mant = $obj_mantenimiento->consultar_preguntas();

if(array_key_exists('ConsultarTodos', $_POST)){
    $obj_mantenimiento = new mantenimiento();
    $mant = $obj_mantenimiento->consultar_preguntas();
}

if(array_key_exists('ConsultarFiltro', $_POST)){
    $obj_mantenimiento = new mantenimiento();
    $mant = $obj_mantenimiento->consultar_preguntas_filtro($_REQUEST['campos'],$_REQUEST['valor']);
}

$nfilas=count($mant);

if ($nfilas > 0){
    print ("<TABLE>\n");
    print ("<TR>\n");
    print ("<TH>ID</TH>\n");
    print ("<TH>Pregunta</TH>\n");
    print ("<TH>Tipo</TH>\n");
    print( "<TH>Edicion</TH>\n");
    print ("</TR>\n");

    foreach ($mant as $resultado){
        print ("<TR>\n");
        print ("<TD>" . $resultado['id'] . "</TD>\n");
        print ("<TD>" . $resultado['pregunta'] . "</TD>\n");
        print ("<TD>" . $resultado['tipo_pregunta'] . "</TD>\n");
        print ("<TD>[ <A HREF='actualizar_preg.php?id_preg=".$resultado['id'] ."'>Editar</A> | <A HREF=".$_SERVER['PHP_SELF'] ."?elim=".$resultado['id'] .">Eliminar</A> ]</TD>\n");
    }
    print ("</TABLE>\n");
}
else{
    print ("No hay preguntas registradas");
}
?>
<BR/>
<A HREF="crear_pregunta.php"> Crear nueva pregunta </A>
<br/><br/>
  <A  HREF ='registro.php'> Pantalla de Registro de Encuestas</A >  &nbsp
<A  HREF ='reportes.php'> Ver Reportes</A >  &nbsp

</BODY>
</HTML>