<HTML LANG="es">
<HEAD>
    <TITLE> Proyecto #1 </TITLE>
    <LINK REL="stylesheet" TYPE="text/css" HREF="css/estilo.css">
</HEAD>
<BODY>
<?PHP
require_once("class/encuesta.php");

//Inicializar arreglo de preguntas ya realizadas -- para no repetir
if (!empty($_REQUEST['arr_realizada']))
    $arr_realizada = $_REQUEST['arr_realizada'];
else
    $arr_realizada = array(0,0,0,0,0,0,0,0,0,0);

//Validar que la pregunta generada no se haya mostrado anteriormente
$existe = TRUE;
while($existe === TRUE)
{
    $id_pregunta = rand(1,20);
    if(in_array($id_pregunta, $arr_realizada) === FALSE)
    {
         $existe = FALSE;
    }
}

//Inicializar el contador
if (!empty($_REQUEST['contador']))
    $contador = $_REQUEST['contador'];
else
    $contador = 1;
//Colocar en el arreglo de preguntas realizadas el id de la pregunta mostrandose actualmente
$arr_realizada[$contador-1] = $id_pregunta;

//Capturamos el nombre del usuario para registrar
$usr = $_COOKIE["user"];

//Capturamos los datos y enviamos respuestas a los metodos para registrar en la BD
if(array_key_exists('enviar', $_POST))
{
    $obj_encuesta = new encuesta();
    
    $contador = $_REQUEST['contador'];
    $id_resp = $_REQUEST['respuesta'];
    foreach($_REQUEST['voto'] as $valor)
    {
        $obj_encuesta->enviar_respuesta($id_resp, $valor, $usr);
    }
}

//Validamos que se continuen desplegando preguntas hasta que se completen 10
if($contador <= 10)
{
    $obj_encuesta = new encuesta();
    $preguntas = $obj_encuesta->obtener_preguntas($id_pregunta);
    $id_responder = $id_pregunta;

    $nfilas=count($preguntas);

    if ($nfilas > 0){
        print ("<H1>Pregunta #".$id_pregunta."</H1>\n");
        foreach ($preguntas as $resultado){
            print ("<TR>\n");
            print ("<TD>" . $resultado['pregunta'] . "</TD>\n");
            //print ("<TD>" . $resultado['tipo_pregunta'] . "</TD>\n");
            print ("<FORM ACTION=".$_SERVER['PHP_SELF'] . " METHOD='POST'>");
            print("<BR/>");
            if ($resultado['tipo_pregunta'] == 1){
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='si' CHECKED>Si<BR>");
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='no'>No<BR>");
            }
            elseif($resultado['tipo_pregunta'] == 2){
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='nadaimportante' CHECKED>Nada Importante<BR>");
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='algoimportante'>Algo Importante<BR>");
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='importante'>Importante<BR>");
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='muyimportante'>Muy Importante<BR>");
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='demimportante'>Demasiado Importante<BR>");
            }
            elseif($resultado['tipo_pregunta'] == 3){
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='samsung'>Samsung<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='Apple'>Apple<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='Huawei'>Huawei<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='sony'>Sony<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='motorolla'>Motorolla<BR>");
            }
            elseif($resultado['tipo_pregunta'] == 4){
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='4a8' CHECKED>De 4 a 8 meses<BR>");
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='8a12'>De 8 a 12 meses<BR>");
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='2a3'>De 2 a 3 años<BR>");
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='3a5'>De 3 a 5 años<BR>");
                print ("<INPUT TYPE='RADIO' NAME='voto[]' VALUE='5a10'>De 5 a 10 años<BR>");
            }
            elseif($resultado['tipo_pregunta'] == 5){
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='crapid'>Carga rápida<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='camara'>Cámara de alta calidad <BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='pantalla'>Pantalla con ultra protección<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='materiales'>Materiales de alta calidad<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='bocinas'>Bocinas potentes<BR>");
            }
            elseif($resultado['tipo_pregunta'] == 6){
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='audifonos'>Audifonos<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='conector'>Conector de corriente de cargador<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='cablecargador'>Cable de cargador<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='protectpantalla'>Protector de pantalla<BR>");
                print ("<INPUT TYPE='CHECKBOX' NAME='voto[]' VALUE='cover'>Protector de teléfono<BR>");
            }
            print("<INPUT TYPE=HIDDEN NAME='contador' VALUE=".($contador+1).">");
            print("<INPUT TYPE=HIDDEN NAME='respuesta' VALUE=".$id_responder.">");
            foreach($arr_realizada as $arreglo)
            {
                print("<INPUT TYPE=HIDDEN NAME='arr_realizada[]' VALUE=".$arreglo.">");
            }
            print ("<BR><INPUT TYPE='SUBMIT' NAME='enviar' VALUE='Votar'>");
            print ("</FORM>");
            
            print("Pregunta #".($contador)." de 10");

            print ("</TR>\n");
        }
    }
    else{
        print ("No hay preguntas con este identificador");
    }
}
else
{
    print ("<BR><BR><P ALIGN='CENTER'>Gracias por participar de la encuesta</P>\n");
    print ("<P ALIGN='CENTER'>[ <A HREF='registro.php'>Regresar al registro</A> ]</P>\n");
    setcookie("user","",time()+60*5);
}
?>
</BODY>
</HTML>


