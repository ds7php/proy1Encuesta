
<HTML LANG="es">
<HEAD>
 <TITLE>Proyecto #1 - Actualizar Pregunta</TITLE>
     <LINK REL="stylesheet" TYPE="text/css" HREF="css/estilo.css">
</HEAD>
<BODY>
<H1>Modificacion de preguntas</H1>
<?php
require_once("class/encuesta_mant.php");



if (!empty($_REQUEST['id_preg']))
    $editar = $_REQUEST['id_preg'];
else
{
    print("No se selecciono ninguna pregunta para modificar");
    $editar=0;
}

if(array_key_exists('enviar', $_POST))
{
    $valor_preg = $_REQUEST['pregunta'];
    $obj_mantenimiento = new mantenimiento();
    $result = $obj_mantenimiento->actualizar_pregunta($editar, $valor_preg);
    if($result){
        print ("<P ALIGN='CENTER'> Se ha modificado tu pregunta correctamente</P>\n");
        print ("<P ALIGN='CENTER'>[ <A HREF='mantenimiento.php'>Volver a la pantalla de mantenimiento</A> ]</P>\n");
    }
    else{
        print ("<A HREF='mantenimiento.php'>Error al modificar su la pregunta</A>\n");
    }
}
else
{
    print ("<FORM ACTION=".$_SERVER['PHP_SELF'] . "?id_preg=".$editar." METHOD='POST'>");
    print ("Ingresa el nuevo valor de la pregunta #".$editar." <INPUT TYPE='text' NAME='pregunta'>")
    ?>
    <INPUT NAME="enviar" VALUE="Actualizar" TYPE="submit" /><BR/>
    </FORM>
    <BR/>
    <?php
}

?>
</BODY>
</HTML>