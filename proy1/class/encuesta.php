<?php

require_once('modelo.php');

class encuesta extends modeloCredencialesBD{

    public function __construct(){
        parent::__construct();
    }

    public function obtener_preguntas($id_preg){
        $instruccion = "CALL sp_listar_pregunta('".$id_preg."')";

        $consulta=$this->_db->query($instruccion);
        $resultado=$consulta->fetch_all(MYSQLI_ASSOC);

        if(!$resultado){
            echo "Fallo al consultar las preguntas";
        }
        else{
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
    }

    public function enviar_respuesta($id_preg,$valor_resp,$usr){
        $instruccion = "CALL sp_guardar_resp('".$id_preg."','".$valor_resp."','".$usr."')";

        $consulta=$this->_db->query($instruccion);
    }

    //Funcion para registrar los datos del usuario
    public function registrar_usuario($nombre,$sexo,$rango_edad,$rango_sal,$provincia){
        $instruccion = "CALL sp_registrar_usr('".$nombre."','".$sexo."','".$rango_edad."','".$rango_sal."','".$provincia."')";

        $consulta=$this->_db->query($instruccion);
    }
}

?>