<?php

require_once('modelo.php');

class mantenimiento extends modeloCredencialesBD{

    public function __construct(){
        parent::__construct();
    }

    public function insertar_pregunta($descripcion,$tipo){
        $instruccion = "CALL sp_insertar_pregunta('".$descripcion."','".$tipo."')";

        $consulta=$this->_db->query($instruccion);

        if($consulta){
            return $consulta;
            $consulta->close();
            $this->_db->close();
        }
    }

    public function consultar_preguntas(){
        $instruccion = "CALL sp_consultar_preg()";

        $consulta=$this->_db->query($instruccion);
        $resultado=$consulta->fetch_all(MYSQLI_ASSOC);

        if($resultado){
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
    }

    public function consultar_preguntas_filtro($campo,$valor){
        $instruccion = "CALL sp_listar_preguntas_filtro('".$campo."','".$valor."')";

        $consulta=$this->_db->query($instruccion);
        $resultado=$consulta->fetch_all(MYSQLI_ASSOC);

        if($resultado){
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
    }

    public function actualizar_pregunta($id,$descrip){
        $instruccion = "CALL sp_actualizar_pregunta('".$descrip."','".$id."')";

        $actualiza=$this->_db->query($instruccion);

        if($actualiza){
            return $actualiza;
            $actualiza->close();
            $this->_db->close();
        }

    }

    public function eliminar_pregunta($id){
        $instruccion = "CALL sp_eliminar_pregunta('".$id."')";

        $consulta=$this->_db->query($instruccion);
        if($consulta){
            return $consulta;
            $consulta->close();
            $this->_db->close();
        }
    }
}

?>