
<?php
require_once('modelo.php');
class reportes extends modeloCredencialesBD{

	public function __construct(){
	parent::__construct();
	}

public function ver_preguntas()
	{
		$instruccion = "CALL sp_ver_preguntas()";
		$consulta=$this->_db->query($instruccion);
		$resultado=$consulta->fetch_all(MYSQLI_ASSOC);

		if($resultado)
		{
		return $resultado;
		$resultado->close();
		$this->_db->close();
		}
	}

 public function consultar_preguntas_filtro(){
        $instruccion = "CALL sp_listar_preguntas_filtro()";

        $consulta=$this->_db->query($instruccion);
        $resultado=$consulta->fetch_all(MYSQLI_ASSOC);

        if($resultado){
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
    }
    public function tipo_sexo(){
        $instruccion = "CALL sp_cantidad_tiposexo()";

        $consulta=$this->_db->query($instruccion);
        $resultado=$consulta->fetch_all(MYSQLI_ASSOC);

        if(!$resultado){
            echo "Fallo al consultar las preguntas";
        }
        else{
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
    }
	
	    public function cant_edad(){
        $instruccion = "CALL sp_cantidad_edad()";

        $consulta=$this->_db->query($instruccion);
        $resultado=$consulta->fetch_all(MYSQLI_ASSOC);

        if(!$resultado){
            echo "Fallo al consultar las preguntas";
        }
        else{
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
    }
	
	   public function cant_provincia(){
        $instruccion = "CALL sp_cantidad_provincia()";

        $consulta=$this->_db->query($instruccion);
        $resultado=$consulta->fetch_all(MYSQLI_ASSOC);

        if(!$resultado){
            echo "Fallo al consultar las preguntas";
        }
        else{
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
    }
	
	
	  public function rang_salario(){
        $instruccion = "CALL sp_cantidad_salario()";

        $consulta=$this->_db->query($instruccion);
        $resultado=$consulta->fetch_all(MYSQLI_ASSOC);

        if(!$resultado){
            echo "Fallo al consultar las preguntas";
        }
        else{
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
    }
}

?>