<HTML LANG="es">
<HEAD>
 <TITLE>Proyecto #1 - Mantenimiento</TITLE>
     <LINK REL="stylesheet" TYPE="text/css" HREF="css/estilo.css">
</HEAD>
<BODY>
<H1>Pantalla de Mantenimiento - Listado de Preguntas</H1>
<?PHP 
require_once("class/encuesta_mant.php");
$obj_mantenimiento = new mantenimiento();
$mant = $obj_mantenimiento->consultar_preguntas();

$nfilas=count($mant);

if ($nfilas > 0){
    print ("<TABLE>\n");
    print ("<TR>\n");
    print ("<TH>ID</TH>\n");
    print ("<TH>Pregunta</TH>\n");
    print ("<TH>Tipo</TH>\n");
    print ("</TR>\n");

    foreach ($mant as $resultado){
        print ("<TR>\n");
        print ("<TD>" . $resultado['id'] . "</TD>\n");
        print ("<TD>" . $resultado['pregunta'] . "</TD>\n");
        print ("<TD>" . $resultado['tipo_pregunta'] . "</TD>\n");
    }
    print ("</TABLE>\n");
}
else{
    print ("No hay preguntas registradas");
}
?>
</BODY>
</HTML>