-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-11-2020 a las 05:06:58
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto1`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualizar_pregunta` (IN `param_preg` VARCHAR(150), IN `param_id` INT)  BEGIN
    
    SET @s = CONCAT ("UPDATE preguntas SET pregunta='", param_preg ,"'\r\n                     WHERE id=", param_id);
    
    PREPARE stmt FROM @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_actualizar_respuesta` (IN `param_id_preg` INT(11), IN `param_valor_resp` VARCHAR(150), IN `param_usuario` VARCHAR(150))  BEGIN
    SET @s = CONCAT ("UPDATE votos SET \r\n                     id_preg    =",param_id_preg,",\r\n                     valor_resp =",param_valor_resp,",\r\n                     usuario    =",param_usuario);           
                    
   PREPARE stmt FROM @S;
   EXECUTE stmt;
   DEALLOCATE PREPARE stmty;
   
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cantidad_edad` ()  BEGIN
    SELECT rango_edad, count(1) From usuarios group by rango_edad;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cantidad_encuestas` ()  BEGIN 
    SELECT id_preg, count(1) From respuestas
    group by id_preg;
	
   
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cantidad_provincia` ()  BEGIN
   SELECT provincia, count(1) From usuarios group by provincia;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cantidad_salario` ()  BEGIN
SELECT rango_salario, count(1) From usuarios group by rango_salario;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cantidad_tiposexo` ()  BEGIN
    SELECT sexo, count(1) From usuarios group by sexo;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_cantidad_usuario` ()  BEGIN
    SELECT count(distinct usuario) from respuestas;
	select count(1) from usuarios;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_preg` ()  BEGIN

    SELECT id, pregunta, tipo_pregunta FROM preguntas;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_consultar_usuario_filtro` (IN `param_campo` VARCHAR(255), IN `param_valor` VARCHAR(255))  BEGIN
    SET @s = CONCAT ("SELECT *
                    FROM usuarios WHERE ", param_campo ,"LIKE CONCAT('%", param_valor ,"%')");
                    
   PREPARE stmt FROM @S;
   EXECUTE stmt;
   DEALLOCATE PREPARE stmty;
   
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_eliminar_pregunta` (IN `param_id` INT)  BEGIN 
    DELETE FROM preguntas WHERE id = param_id; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_guardar_resp` (IN `param_idpreg` INT, IN `param_val` VARCHAR(150), IN `param_usr` VARCHAR(150))  BEGIN
    INSERT INTO respuestas (id_preg, valor_resp, usuario)
    VALUES (param_idpreg, param_val , param_usr);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_pregunta` (IN `param_preg` VARCHAR(255), IN `param_tipo` INT)  BEGIN
    INSERT INTO preguntas (pregunta, tipo_pregunta)
    VALUES (param_preg , param_tipo);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_pregunta` (IN `param_id` INT)  BEGIN
    SET @s = CONCAT("SELECT id, pregunta, tipo_pregunta FROM preguntas\r\n                    WHERE id = '", param_id ,"'");
    PREPARE stmt FROM @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_preguntas_filtro` (IN `param_campo` VARCHAR(255), IN `param_valor` VARCHAR(255))  BEGIN
    
    SET @S = CONCAT("SELECT id, pregunta, tipo_pregunta\r\n                    FROM preguntas WHERE ", param_campo, " LIKE CONCAT('%", param_valor ,"%')");
    PREPARE stmt FROM @s;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_respuestas` ()  BEGIN 
SELECT id_preg, valor_resp,usuario  FROM respuestas;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_registrar_usr` (IN `param_usr` VARCHAR(150), IN `param_sexo` CHAR(1), IN `param_edad` VARCHAR(100), IN `param_salario` VARCHAR(100), IN `param_prov` VARCHAR(100))  BEGIN
    INSERT INTO usuarios (nombre, sexo, rango_edad, rango_salario, provincia)
    VALUES (param_usr, param_sexo , param_edad, param_salario, param_prov);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ver_preguntas` ()  BEGIN
select a.id_preg, count(1), (select pregunta from preguntas b where b.id = a.id_preg) `descripcion` from respuestas a group by id_preg;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `id` int(11) NOT NULL,
  `pregunta` varchar(150) NOT NULL,
  `tipo_pregunta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`id`, `pregunta`, `tipo_pregunta`) VALUES
(1, '¿Es usted usuario o poseedor de un teléfono móvil?', 1),
(2, '¿Qué tan importante considera que es un teléfono móvil en su vida? ', 2),
(3, '¿Conoce usted algunas de las compañías de teléfonos móviles actuales?', 1),
(4, '¿Usted es poseedor de un teléfono móvil?', 1),
(5, '¿Qué marca de teléfono móvil posee actualmente de las siguientes? ', 3),
(6, '¿Usted ha poseído alguna de las siguientes marcas de teléfonos móviles? ', 3),
(7, '¿Cada cuanto usted cambia un teléfono móvil?', 4),
(8, '¿En la compra de un teléfono móvil, usted tomaría en cuenta alguna de las siguientes opciones?', 5),
(9, '¿Considera usted que todos los teléfonos móviles deberían ser calidad-precio?', 1),
(10, '¿Cuál consideraría usted que es la marca de teléfonos móviles líder actual en el mercado?', 3),
(11, '¿Cuáles de las siguientes compañías de teléfonos móviles cree usted que ofrece los mejores teléfonos', 3),
(12, '¿Qué cree usted que promocionan las grandes compañías de teléfonos móviles, que no tienen sus teléfonos después?', 5),
(13, '¿Cree usted que las compañías de teléfonos móviles deberían actualizar sus modelos por año?', 1),
(14, '¿Considera usted que los teléfonos actuales son de igual calidad de durabilidad que los antiguos?', 1),
(15, '¿Accedería usted a un contrato de renovación de teléfono gratis cada año por 10 años, considerando que no puede usar los de otras compañías?', 1),
(16, '¿Es importante para usted que un teléfono móvil tenga cámaras de alta calidad?', 2),
(17, '¿Es un factor muy necesario para usted que un teléfono móvil tenga mucha memoria RAM?', 2),
(18, '¿Qué tan importante en duración debería ser la batería de los teléfonos móviles según su opinión?', 2),
(19, '¿Qué piensa usted que debería traer un teléfono móvil en su caja al momento de su compra?', 6),
(20, '¿Es para usted importante que los teléfonos actuales tengan medidas de seguridad como lector de huellas en pantallas y reconocimiento facial?', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE `respuestas` (
  `id_preg` int(11) NOT NULL,
  `valor_resp` varchar(150) NOT NULL,
  `usuario` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`id_preg`, `valor_resp`, `usuario`) VALUES
(1, 'si', ''),
(1, 'no', 'carlos'),
(1, 'si', 'Edgardo'),
(2, 'muyimportante', ''),
(2, 'demimportante', 'carlos'),
(2, 'importante', 'isbeth'),
(3, 'si', ''),
(3, 'si', 'carlos'),
(3, 'si', 'Edgardo'),
(4, 'si', ''),
(4, 'si', 'Bryan'),
(4, 'si', 'edgardo2'),
(5, '', ''),
(5, 'Huawei', 'Bryan'),
(5, 'Huawei', 'carlos'),
(5, 'Apple', 'isbeth'),
(6, 'Huawei', 'Edgardo'),
(6, 'Apple', 'isbeth'),
(7, '3a5', 'Bryan'),
(7, '8a12', 'carlos'),
(8, 'bocinas', ''),
(8, '', 'Bryan'),
(8, 'crapid', 'carlos'),
(8, 'camara', 'Edgardo'),
(8, 'pantalla', 'edgardo2'),
(9, 'no', ''),
(9, 'si', 'Edgardo'),
(10, 'Huawei', ''),
(10, 'Apple', 'Bryan'),
(10, 'Huawei', 'edgardo2'),
(10, 'Apple', 'isbeth'),
(11, 'Huawei', 'edgardo2'),
(11, 'Apple', 'isbeth'),
(12, 'bocinas', ''),
(12, 'bocinas', 'edgardo2'),
(13, 'no', ''),
(13, 'si', 'Bryan'),
(13, 'si', 'carlos'),
(13, 'si', 'edgardo2'),
(14, 'si', 'Bryan'),
(14, 'si', 'carlos'),
(14, 'si', 'Edgardo'),
(14, 'si', 'edgardo2'),
(14, 'si', 'isbeth'),
(15, 'no', ''),
(15, 'si', 'Bryan'),
(15, 'si', 'carlos'),
(15, 'si', 'Edgardo'),
(15, 'si', 'isbeth'),
(16, 'nadaimportante', ''),
(16, 'importante', 'isbeth'),
(17, 'nadaimportante', ''),
(17, 'nadaimportante', 'Edgardo'),
(17, 'demimportante', 'edgardo2'),
(17, 'importante', 'isbeth'),
(19, 'protectpantalla', ''),
(19, 'protectpantalla', 'carlos'),
(19, 'audifonos', 'Edgardo'),
(20, 'nadaimportante', 'Bryan'),
(20, 'muyimportante', 'isbeth');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `nombre` varchar(150) NOT NULL,
  `sexo` char(1) NOT NULL,
  `rango_edad` varchar(10) NOT NULL,
  `rango_salario` varchar(100) NOT NULL,
  `provincia` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`nombre`, `sexo`, `rango_edad`, `rango_salario`, `provincia`) VALUES
('Bryan', 'M', '-18', '-$1000', 'Ciudad de Panamá'),
('Edgardo', 'M', '25-35', '$2000-$3000', 'Ciudad de Panamá'),
('Bryan', 'M', '-18', '-$1000', 'Ciudad de Panamá'),
('', 'M', '-18', '-$1000', ''),
('', 'M', '-18', '-$1000', ''),
('edgardo2', 'M', '25-35', '-$1000', 'panama'),
('carlos', 'M', '50+', '$3000-$4000', 'Veraguas'),
('isbeth', 'F', '18-25', '-$1000', 'darien');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD UNIQUE KEY `idx_preguntas` (`id`);

--
-- Indices de la tabla `respuestas`
--
ALTER TABLE `respuestas`
  ADD UNIQUE KEY `idx_resp_k1` (`id_preg`,`usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
